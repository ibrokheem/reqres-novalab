import "./login.css";
import { useRef } from "react";
import { useNavigate } from "react-router-dom";

const Login = ({ setToken }) => {
  const emailRef = useRef();
  const passwordRef = useRef();
  const navigate = useNavigate();

  let emailValue = "";
  let passwordValue = "";

  async function login() {
    return fetch("https://reqres.in/api/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: emailValue,
        password: passwordValue,
      }),
    }).then((response) => response.json());
  }

  async function handleFormSubmit(evt) {
    evt.preventDefault();
    emailValue = emailRef.current.value;
    passwordValue = passwordRef.current.value;

    let response = await login();

    if (response.token) {
      alert("You have successfully logged in. Now You can visit profile page");
      emailRef.current.value = "";
      passwordRef.current.value = "";
      localStorage.setItem("token", response.token);
      setToken(response.token);
      navigate("/");
    } else {
      alert("wrong email or password");
    }
  }

  return (
    <main>
      <div className="login">
        <div className="container login__container">
          <h2 className="login__headline">Login</h2>
          <form
            className="login__form"
            method="post"
            onSubmit={handleFormSubmit}
            autoComplete="off"
          >
            <input
              className="login__input"
              type="email"
              ref={emailRef}
              placeholder="Email"
              defaultValue={"eve.holt@reqres.in"}
              required
            />
            <input
              className="login__input"
              type="password"
              ref={passwordRef}
              placeholder="Password"
              defaultValue={"cityslicka"}
              required
            />
            <button className="login__btn-submit" type="submit">
              Submit
            </button>
          </form>
        </div>
      </div>
    </main>
  );
};

export default Login;
