import UsersList from "../UsersList/UsersList";

const Home = ({ users }) => {
  return (
    <main>
      <UsersList users={users} />
    </main>
  );
};

export default Home;
