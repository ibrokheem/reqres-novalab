import "./header.css";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import SweetAlert from "react-bootstrap-sweetalert";
import { useState } from "react";

const Header = ({ token, setToken }) => {
  const navigate = useNavigate();
  const [logOutModal, setLogOutModal] = useState(false);

  function logOut(evt) {
    evt.preventDefault();
    setToken("");
    localStorage.removeItem("token");
    setLogOutModal(false);
    navigate("/");
  }

  return (
    <header className="site-header">
      <div className="container site-header__container">
        <Link to="/">
          <img
            className="main-logo"
            src="https://picsum.photos/100/100"
            alt="logo"
            width={100}
            height={100}
          />
        </Link>

        <nav className="navbar">
          <ul className="navbar__list">
            <li className="navbar__item">
              <Link className="navbar__link" to="/">
                Home
              </Link>
            </li>

            {token ? (
              <>
                <li className="navbar__item">
                  <Link className="navbar__link" to="/profile">
                    Profile
                  </Link>
                </li>

                <li
                  className="navbar__item"
                  onClick={() => setLogOutModal(true)}
                >
                  Log out
                </li>
              </>
            ) : (
              <>
                <li className="navbar__item">
                  <Link className="navbar__link" to="/register">
                    Register
                  </Link>
                </li>

                <li className="navbar__item">
                  <Link className="navbar__link" to="/login">
                    Login
                  </Link>
                </li>
              </>
            )}
          </ul>
        </nav>
      </div>

      <SweetAlert
        show={logOutModal}
        onCancel={() => setLogOutModal(false)}
        onConfirm={logOut}
        style={{
          padding: "20px",
          backgroundColor: "white",
          borderRadius: "20px",
        }}
        confirmBtnStyle={{ display: "none" }}
        customClass="logout-modal"
        title=""
      >
        <div className="logout-modal__inner">
          <h3 className="logout-modal__title">Do you really want to log out</h3>
          <button
            className="logout-modal__btn-exit"
            onClick={() => setLogOutModal(false)}
          >
            X
          </button>
          <div className="logout-modal__btns-wrapper">
            <button className="logout-modal__btn-confirm" onClick={logOut}>
              YES
            </button>
            <button
              className="logout-modal__btn-cancel"
              onClick={() => setLogOutModal(false)}
            >
              NO
            </button>
          </div>
        </div>
      </SweetAlert>
    </header>
  );
};

export default Header;
