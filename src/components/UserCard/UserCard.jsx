import "./usercard.css";

const UserCard = ({ user }) => {
  return (
    <div className="user-card">
      <img
        className="user-card__avatar"
        src={user.avatar}
        alt="user avatar"
        width={290}
        height={290}
      />
      <p className="user-card__info">
        Id: <strong>{user.id}</strong>
      </p>
      <p className="user-card__info">
        Email: <strong>{user.email}</strong>
      </p>
      <p className="user-card__info">
        First Name: <strong>{user.first_name}</strong>
      </p>
      <p className="user-card__info">
        Last Name: <strong>{user.last_name}</strong>
      </p>
    </div>
  );
};

export default UserCard;
