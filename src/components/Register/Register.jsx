import "./register.css";
import { useRef } from "react";
import { useNavigate } from "react-router-dom";

const Register = ({ setToken }) => {
  const emailRef = useRef();
  const passwordRef = useRef();
  const navigate = useNavigate();

  let emailValue = "";
  let passwordValue = "";

  async function register() {
    return fetch("https://reqres.in/api/register", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: emailValue,
        password: passwordValue,
      }),
    }).then((response) => response.json());
  }

  async function handleFormSubmit(evt) {
    evt.preventDefault();
    emailValue = emailRef.current.value;
    passwordValue = passwordRef.current.value;

    let response = await register();

    if (response.token) {
      alert("You have successfully registered. Now You can visit profile page");
      localStorage.setItem("token", response.token);
      setToken(response.token);
      navigate("/");
    } else {
      alert("wrong email or password");
    }
  }

  return (
    <main>
      <div className="register">
        <div className="container register__container">
          <form
            className="register__form"
            method="post"
            onSubmit={handleFormSubmit}
            autoComplete="off"
          >
            <h2 className="register__headline">register</h2>
            <input
              className="register__input"
              type="email"
              ref={emailRef}
              placeholder="Email"
              defaultValue={"eve.holt@reqres.in"}
              required
            />
            <input
              className="register__input"
              type="password"
              ref={passwordRef}
              placeholder="Password"
              defaultValue={'pistol'}
              required
            />
            <button className="register__btn-submit" type="submit">
              Submit
            </button>
          </form>
        </div>
      </div>
    </main>
  );
};

export default Register;
