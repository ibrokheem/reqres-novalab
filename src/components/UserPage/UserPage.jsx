import "./user-page.css";
import { useState, useEffect, useRef } from "react";
import { useParams } from "react-router-dom";

const UserPage = () => {
  const { id } = useParams();
  const [userData, setUserData] = useState({});
  const inputRef = useRef();
  let inputValue = "";

  async function getUserData() {
    const response = await fetch(`https://reqres.in/api/users/${id}`);
    const data = await response.json();
    setUserData(data.data);
  }

  function editUserData() {
    fetch(`https://reqres.in/api/users/${id}`, {
      method: "PATCH",
      body: JSON.stringify({
        ...userData,
        first_name: inputValue,
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    })
      .then((response) => response.json())
      .then((json) => setUserData(json));

    getUserData();
  }

  function onInputChange(evt) {
    evt.preventDefault();
    inputValue = inputRef.current.value;
    inputRef.current.value = "";
    console.log(inputValue);
    editUserData();
  }

  useEffect(() => {
    getUserData();
  }, []);

  return (
    <main>
      <div className="user-page">
        <div className="container user-page__container">
          <div className="user-page__top">
            <div className="user-page__top-inner">
              <img className="user-page__avatar" src={userData.avatar} alt="" />
              <div className="user-page__info">
                <h2 className="user-page__username">
                  {userData.first_name} {userData.last_name}
                </h2>
                <p className="user-page__id">Id: {userData.id}</p>
              </div>
            </div>
            <p className="user-page__email">{userData.email}</p>
          </div>

          <form
            className="user-page__form"
            method="Post"
            onSubmit={onInputChange}
          >
            <h3 className="user-page__form-title">Edit User's name</h3>
            <input
              className="user-page__input"
              ref={inputRef}
              type="text"
              placeholder="Name"
            />
            <button className="user-page__btn-submit" type="submit">
              submit
            </button>
          </form>
        </div>
      </div>
    </main>
  );
};

export default UserPage;
