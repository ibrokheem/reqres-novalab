import { Link } from "react-router-dom";
import UserCard from "../UserCard/UserCard";
import "./users-list.css";
const UsersList = ({ users }) => {
  return (
    <div className="container">
      <ul className="users-list">
        {users.map((user) => (
          <li className="users-list__item" key={user.id}>
            <Link className="users-list__link" to={`userpage/${user.id}`}>
              <UserCard user={user}></UserCard>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default UsersList;
