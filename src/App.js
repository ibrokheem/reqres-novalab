import "./App.css";
import { BrowserRouter, Route, Routes} from "react-router-dom";
import Header from "./components/Header/Header";
import Home from "./components/Home/Home";
import Login from "./components/Login/Login";
import Profile from "./components/Profile/Profile";
import UserPage from "./components/UserPage/UserPage";
import Register from "./components/Register/Register";
const { useState, useEffect } = require("react");

const App = () => {
  const [users, setUsers] = useState([]);

  const [token, setToken] = useState(localStorage.getItem("token") || "");

  async function getUsers() {
    const response = await fetch("https://reqres.in/api/users?page=1");
    const data = await response.json();
    const response2 = await fetch("https://reqres.in/api/users?page=2");
    const data2 = await response2.json();

    setUsers([...data.data, ...data2.data]);
  }

  useEffect(() => {
    getUsers();
  }, []);

  return (
    <>
      <BrowserRouter>
        <Header token={token} setToken={setToken} />

        <Routes>
          <Route exact path="/" element={<Home users={users} />} />

          <Route exact path="/login" element={<Login setToken={setToken} />} />

          <Route path="/register" element={<Register setToken={setToken} />} />

          <Route path="userpage/:id" element={<UserPage />} />

          <Route path="/profile" element={<Profile />} />
        </Routes>
      </BrowserRouter>
    </>
  );
};

export default App;
